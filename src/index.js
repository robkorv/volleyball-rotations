import "normalize.css/normalize.css";
import "./main.scss";
import { SVG } from "@svgdotjs/svg.js";

const courtColor = "#9fa9a8";
const courtLineColor = "#f1c68c";
const courtLineWidth = 10;
const courtLineStrokeOptions = { color: courtLineColor, width: courtLineWidth };

const fieldPositionStrokeOptions = { color: "#000000", width: 1 };

const drawElement = document.getElementById("draw");
const draw = SVG().addTo(drawElement);
const court = draw.rect().fill(courtColor).stroke(courtLineStrokeOptions);
const threeMeterLine = draw.line().stroke(courtLineStrokeOptions);

class FieldPosition {
    constructor(draw, number, beforeThreeMeterLine) {
        this.rect = draw
            .rect()
            .fill({ opacity: 0 })
            .stroke(fieldPositionStrokeOptions);
        this.text = draw.text(number.toString());
        this.beforeThreeMeterLine = beforeThreeMeterLine;
    }
}

const fieldPositions = [];
[4, 3, 2].forEach((number) => {
    fieldPositions.push(new FieldPosition(draw, number, true));
});
[5, 6, 1].forEach((number) => {
    fieldPositions.push(new FieldPosition(draw, number, false));
});

const handleResize = () => {
    draw.size(drawElement.clientWidth, drawElement.clientWidth);
    court.size(drawElement.clientWidth, drawElement.clientWidth);

    let threeMeterPoint = court.width() / 3;
    threeMeterLine.plot(
        0,
        threeMeterPoint,
        drawElement.clientWidth,
        threeMeterPoint
    );

    let fieldPositionWidth = (court.width() - 2 * courtLineWidth) / 3;
    for (let i = 0; i < fieldPositions.length; i++) {
        let fieldPosition = fieldPositions[i];
        let fieldPositionHeight = 0;
        let fieldPositionX = 0;
        let fieldPositionY = 0;

        if (fieldPosition.beforeThreeMeterLine) {
            fieldPositionHeight = threeMeterPoint - courtLineWidth * 2;
            fieldPositionX = courtLineWidth + fieldPositionWidth * i;
            fieldPositionY = courtLineWidth;
        } else {
            fieldPositionHeight = threeMeterPoint * 2 - courtLineWidth * 2;
            fieldPositionX = courtLineWidth + fieldPositionWidth * (i - 3);
            fieldPositionY = threeMeterPoint + courtLineWidth;
        }

        fieldPosition.text.move(fieldPositionX, fieldPositionY);
        fieldPosition.rect
            .size(fieldPositionWidth, fieldPositionHeight)
            .move(fieldPositionX, fieldPositionY);
    }
};
handleResize();
window.addEventListener("resize", handleResize);
